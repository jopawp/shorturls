class ShorturlsController < ApplicationController
	def show_all
		@shorturls = Shorturl.all.order(:created_at).reverse_order
	end

	def new
		@shorturls = Shorturl.all.order(:created_at).reverse_order
		@shorturl = Shorturl.new
		render 'index'
	end

	def create
		if params[:shorturl][:long] == "" || params[:shorturl][:title] == "" || Shorturl.find_by(long: params[:shorturl][:long]) != nil
			flash[:error] = "Duplicate or missing title or URL."
			redirect_to root_path
			return
		end

		begin
			r = rand(62**4)
		end while Shorturl.find_by(short: encode(r))
		
		s = Shorturl.create(params.require(:shorturl).permit(:title, :long, :short, :clicks))
		s.short = encode(r)
		s.save
		
		flash[:newurl] = "#{url_for(root_url)}#{s.short}"
		redirect_to root_path
	end

	def show
		if params[:short] != nil && Shorturl.find_by(short: params[:short]).long != ""
			s = Shorturl.find_by(short: params[:short])
			s.clicks = s.clicks + 1
			s.save
			redirect_to s.long
		else
			flash[:error] = "Unable to find URL."
			redirect_to root_path
		end
	end

	private
	def encode(d)
	
		symbols = [
			"s","q","S","O","K","b","C","t","d","b",
			"l","q","O","Q","D","r","z","Q","H","8",
			"f","8","L","e","w","g","I","L","W","H",
			"H","W","N","4","M","L","1","S","W","h",
			"z","k","5","7","s","F","q","P","Y","w",
			"T","i","F","w","Q","B","8","e","V","c",
			"z","P"
		]
		base = symbols.length
		exp = 1
		tmp = d
		e = ""
		while (tmp / (base ** exp)) != 0
			exp += 1
		end
		while  exp > 0
			if tmp > base
				e << symbols[(tmp / (base ** exp))]
				tmp = tmp - ((tmp / (base ** exp)) * (base ** exp))
				exp -= 1
			else
				tmp = tmp % base
				e << symbols[tmp]
				exp -= 1
			end
		end
		return e
	end

end
