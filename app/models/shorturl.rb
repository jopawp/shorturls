class Shorturl < ActiveRecord::Base
	after_initialize :init

	def init
		self.clicks ||= 0
	end
end
