require 'rails_helper'

RSpec.describe "Shorturls", type: :request do
  describe "GET /" do
    it "loads the index page successfully" do
      get root_path
      expect(response).to have_http_status(200)
    end
  end
end
