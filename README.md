# ShortURLs README

   A  short URL generator with redirection.

## Contributors

   * Matt Weidner <jopawp@gmail.com>

Things you may want to cover:

   * Developed with Ruby 2.2.2 and Rails 4
   * Uses a Postgresql database.
   * Tested on Heroku for a short time.