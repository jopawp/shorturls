#!/usr/bin/ruby


symbols = [
			 "A","B","C","D","E","F","G","H","I","J",
			 "K","L","M","N","O","P","Q","R","S","T",
			 "U","V","W","X","Y","Z","a","b","c","d",
			 "e","f","g","h","i","j","k","l","m","n",
			 "o","p","q","r","s","t","u","v","w","x",
			 "y","z","0","1","2","3","4","5","6","7",
			 "8","9"
		]

random_symbols = symbols.clone

i = 0
while i < 62**2
	random_symbols[rand(62)] = symbols[rand(62)]
	i += 1
end
i = 0
puts "symbols = ["
while i < 62
	if i > 0
		print ","
		if i % 10 == 0
			puts ""
		end
	end
	print '"' + "#{random_symbols[i]}" + '"'
	i += 1
end
puts ""
puts "]"
