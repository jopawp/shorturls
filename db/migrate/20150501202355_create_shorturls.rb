class CreateShorturls < ActiveRecord::Migration
  def change
    create_table :shorturls do |t|
      t.string :short
      t.string :long
      t.integer :clicks
      t.string :title

      t.timestamps null: false
    end
  end
end
